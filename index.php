<!doctype html>

<html lang="ru">

<head>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1"/>
  <link rel="stylesheet" type="text/css" href="style.css">
  <title>Index</title>
</head>

<form method="post" class="content" action="" autocomplete="off">
    <a id="link">
      <h2>Форма</h2>
    </a>
    <label>
      Текстовое поле для ввода имени:<br/>
      <input name="name" <?php if (isset($_COOKIE["wrong_name"])) echo "class='red'" ?> <?PHP if (isset($_COOKIE["name"])) echo "value=", $_COOKIE["name"]?> />
      <?php if (isset($_COOKIE["wrong_name"])) echo "<br/>Неверное имя. Имя должно начинаться с заглавной буквы, содержать только латинские буквы и состоять из одного слова." ?>
    </label><br/>
    <label >
      Текстовое поле email:<br/>
      <input name="email" <?php if (isset($_COOKIE["wrong_email"])) echo "class='red'" ?> <?PHP if (isset($_COOKIE["email"])) echo "value=", $_COOKIE["email"]?> />
      <?php if (isset($_COOKIE["wrong_email"])) echo "<br/>Неверная почта. Формат почты: <имя>@<домен>.<сервер>, причём имя может содержать только латинские символы, цифры и знаки -, _, домен - латинские символы и цифры, а сервер - латинские символы.<br/>" ?>
    </label><br/>
    <label>
      Год рождения:<br/>
      <select size="5" name="birthyear">
        <option value="1996" <?php if (isset($_COOKIE["birthyear"])) if ($_COOKIE["birthyear"]=="1996") echo "selected"?> >1996</option>
        <option value="1997" <?php if (isset($_COOKIE["birthyear"])) if ($_COOKIE["birthyear"]=="1997") echo "selected"?> >1997</option>
        <option value="1998" <?php if (isset($_COOKIE["birthyear"])) if ($_COOKIE["birthyear"]=="1998") echo "selected"?> >1998</option>
        <option value="1999" <?php if (isset($_COOKIE["birthyear"])) if ($_COOKIE["birthyear"]=="1999") echo "selected"?> >1999</option>
        <option value="2000" <?php if (isset($_COOKIE["birthyear"])) if ($_COOKIE["birthyear"]=="2000") echo "selected"?> >2000</option>
        <option value="2001" <?php if (isset($_COOKIE["birthyear"])) if ($_COOKIE["birthyear"]=="2001") echo "selected"?> >2001</option>
        <option value="2002" <?php if (isset($_COOKIE["birthyear"])) if ($_COOKIE["birthyear"]=="2002") echo "selected"?> >2002</option>
      </select>
    </label><br/>
    Пол:<br/>
    <label><input type="radio" name="sex" value="M" <?php if (isset($_COOKIE["sex"])) if ($_COOKIE["sex"]=="M") echo "checked" ?>/> Мужской</label>
    <label><input type="radio" name="sex" value="F" <?php if (isset($_COOKIE["sex"])) if ($_COOKIE["sex"]=="F") echo "checked" ?>/> Женский</label><br/>
    Количество конечностей:<br/>
    <label><input type="radio" name="limb" value="1" <?php if (isset($_COOKIE["limb"])) if ($_COOKIE["limb"]=="1") echo "checked"?> /> 1 </label>
    <label><input type="radio" name="limb" value="2" <?php if (isset($_COOKIE["limb"])) if ($_COOKIE["limb"]=="2") echo "checked"?> /> 2 </label>
    <label><input type="radio" name="limb" value="4" <?php if (isset($_COOKIE["limb"])) if ($_COOKIE["limb"]=="4") echo "checked"?> /> 4 </label>
    <label><input type="radio" name="limb" value="6" <?php if (isset($_COOKIE["limb"])) if ($_COOKIE["limb"]=="6") echo "checked"?> /> 6 </label>
    <label><input type="radio" name="limb" value="8" <?php if (isset($_COOKIE["limb"])) if ($_COOKIE["limb"]=="8") echo "checked"?> /> 8 </label><br/>
    Сверхспособности:<br/>
    <select name="powers[]" multiple="multiple" autocomplete="off">
      <option value="1" <?php if (isset($_COOKIE["powers"])) if (in_array("1", $_COOKIE["powers"])) echo "selected='selected'"?> >Бессмертие</option>
      <option value="2" <?php if (isset($_COOKIE["powers"])) if (in_array("2", $_COOKIE["powers"])) echo "selected='selected'"?> >Прохождение сквозь стены</option>
      <option value="3" <?php if (isset($_COOKIE["powers"])) if (in_array("3", $_COOKIE["powers"])) echo "selected='selected'"?> >Левитация</option>
      <option value="4" <?php if (isset($_COOKIE["powers"])) if (in_array("4", $_COOKIE["powers"])) echo "selected='selected'"?> >Отсутствие потребности во сне</option>
    </select><br/>
    <label>
      Биография:<br/>
      <textarea name="biography"><?php if (isset($_COOKIE["biography"])) echo $_COOKIE["biography"] ?></textarea>
    </label><br/>
    <label>
      <input type="checkbox" name="confirm"/>
      С контрактом ознакомлен<br/>
    </label>
    <input name="send" type="submit" value="Отправить"/>
    </br></br>
    
    <?php
    if (!empty($_COOKIE["complete"]) && $_COOKIE["complete"]==true && (empty($_COOKIE["refresh"]) || $_COOKIE["refresh"]==0)) {
      echo "Запрос был успешно отправлен! \n";
      setcookie("complete", false);
      if (isset($_COOKIE['new_user'])) {
        $db_host = 'localhost'; $db_user = 'u20967'; $db_password = '7306510';
        $bd = new PDO("mysql:host=$db_host; dbname=$db_user", $db_user, $db_password, array(PDO::ATTR_PERSISTENT => true));
        $id_user = $_COOKIE["id_user"];

        echo "<br/>Поздравляем, ваш первый герой сохранён! Однако для того, чтобы с ним можно было работать и в следующей сессии, пожалуйста, запомните ваши логин и пароль.";
        echo "<br/>Логин: ", $bd->query("SELECT login FROM users WHERE id_user=$id_user")->fetch()[0];
        echo "<br/>Пароль: ", $bd->query("SELECT password FROM users WHERE id_user=$id_user")->fetch()[0];
        setcookie("new_user", "", time()-1);
        setcookie("first", 1);
      }
    } else {
    if (isset($_COOKIE["first"]) && $_COOKIE["first"]==0) {
      if (empty($_COOKIE["name"])) echo "Поле с именем не заполнено.<br/>";
      if (empty($_COOKIE["email"])) echo "Поле с электронной почтой не заполнено.<br/>";
      if (empty($_COOKIE["birthyear"])) echo "Поле с датой рождения не заполнено.<br/>";
      if (empty($_COOKIE["sex"])) echo "Поле с полом не заполнено.<br/>";
      if (empty($_COOKIE["limb"])) echo "Поле с количеством конечностей не заполнено.<br/>";
      if (empty($_COOKIE["powers"])) echo "Поле с суперспособностями не заполнено.<br/>";
      if (empty($_COOKIE["confirm"])) echo "Вы должны ознакомиться с контрактом.<br/>";
    } else {
      setcookie("first", 0);
    }}
    ?>

    <?php
  header('Content-Type: text/html; charset=UTF-8');
  setlocale(LC_ALL, 'Russian_Russia.65001');

  if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST["send"]))
  try{
    $check = true;
    if (!empty($_POST['name'])) {
      setcookie("name", $_POST["name"], time()+31536000);
      if (preg_match("/^[A-Z]{1}[a-z]{0,}$/", $_POST['name'])==0) {
        setcookie("wrong_name", true, time()+31536000);
        $check = false;
      } else {
        if (isset($_COOKIE["wrong_name"])) setcookie("wrong_name", false, time()-1);
      }
    } else {
      setcookie("name", "", time()-1);
      $check = false;
    }
    if (!empty($_POST['email'])) {
      setcookie("email", $_POST["email"], time()+31536000);
      if (preg_match("/^[\-_A-Za-z0-9]{1,}@[A-Za-z0-9]{1,}.[A-Za-z]{2,}$/", $_POST['email'])==0) {
        setcookie("wrong_email", true, time()+31536000);
        $check = false;
      } else {
        if (isset($_COOKIE["wrong_email"])) setcookie("wrong_email", false, time()-1);
      }
    } else {
      setcookie("email", "", time()-1);
      $check = false;
    }
    if (!empty($_POST['birthyear'])) {
      setcookie("birthyear", $_POST["birthyear"], time()+31536000);
    } else {
      setcookie("birthyear", "", time()-1);
      $check = false;
    }
    if (!empty($_POST['sex'])) {
      setcookie("sex", $_POST["sex"], time()+31536000);
    } else {
      setcookie("sex", "", time()-1);
      $check = false;
    }
    if (!empty($_POST['limb'])) {
      setcookie("limb", $_POST["limb"], time()+31536000);
    } else {
      setcookie("limb", "", time()-1);
      $check = false;
    }
    if (isset($_COOKIE["powers"]))
        for ($i = 0; $i<count($_COOKIE["powers"]); $i++) {
          setcookie("powers[$i]", "", time()-1);
        }
    if (!empty($_POST['powers'])) {
      for ($i = 0; $i<count($_POST["powers"]); $i++) {
        setcookie("powers[$i]", $_POST["powers"][$i], time()+31536000);
      }
    } else {
      $check = false;
    }
    if (!empty($_POST['biography'])) {
      setcookie("biography", $_POST["biography"], time()+31536000);
    } else {
      setcookie("biography", "", time()-1);
      $check = false;
    }
    if (empty($_POST['confirm'])) {
      $check = false;
    }

    if ($check==true) {
      setcookie("name", "", time()-1);
      setcookie("email", "", time()-1);
      setcookie("birthyear", "", time()-1);
      setcookie("sex", "", time()-1);
      setcookie("limb", "", time()-1);
      if (isset($_COOKIE["powers"])) for ($i = 0; $i<count($_COOKIE["powers"]); $i++) {
        setcookie("powers[$i]", "", time()-1);
      }
      setcookie("biography", "", time()-1);
    }
    
    if ($check==false) {
      header("Refresh: 0");
      exit;
    }
    
    $db_host = 'localhost'; $db_user = 'u20967'; $db_password = '7306510';
    $bd = new PDO("mysql:host=$db_host; dbname=$db_user", $db_user, $db_password, array(PDO::ATTR_PERSISTENT => true));

    $heroes = $bd->prepare("INSERT INTO heroes SET name = ?, email = ?, birthyear = ?, sex = ?, limb = ?, biography = ?");
    $heroes -> execute([$_POST['name'], $_POST['email'], $_POST['birthyear'], $_POST['sex'], $_POST['limb'], $_POST['biography']]);
    $id_hero = $bd->lastInsertId();

    $powers_sets = $bd->prepare("INSERT INTO powers_sets SET id_hero = ?, id_power = ?");
    for ($i = 0; $i<count($_POST["powers"]); $i++) {
      $powers_sets -> execute([$id_hero, $_POST['powers'][$i]]);
    }

    if (empty($_COOKIE['id_user'])) {
      $login = substr(str_shuffle('abdefhiknrstyzABDEFGHKNQRSTYZ'), 0, 8);
      $password = substr(str_shuffle('abdefhiknrstyzABDEFGHKNQRSTYZ1234567890'), 0, 12);
      $users = $bd->prepare("INSERT INTO users SET login = ?, password = ?");
      $users -> execute([$login, $password]);
      $id_user = $bd->lastInsertId();
      setcookie("id_user", $id_user, time()+31536000);
      setcookie("new_user", true, time()+31536000);
    } else {
      $id_user = $_COOKIE["id_user"];
    }

    $heroes_sets = $bd->prepare("INSERT INTO heroes_sets SET id_hero = ?,  id_user = ?");
    $heroes_sets -> execute([$id_hero, $id_user]);

    header("Refresh: 0");
    setcookie("complete", true);
  } catch(PDOException $e){
    echo "Произошла ошибка при отправке запроса!";
    print('Error : ' . $e->getMessage());
  }
?>
</form>

<div class="content">
<form <?php if (empty($_COOKIE["id_user"])) echo "hidden=true"?>method="post" action="editor.php">
  <button>Редактирование героев</button>
</form>
<form <?php if (empty($_COOKIE["id_user"])) echo "hidden=true"?> method="post" action="logout.php">
  <button>Выйти из аккаунта</button>
</form>
</div>

<form <?php if (isset($_COOKIE["id_user"])) echo "hidden=true"?> class="content" method="post" action="login.php">
<h2>Если у вас уже есть аккаунт:</h2><br/>
<label>
  Логин: <input name="login"/>
  Пароль: <input name="password"/>
  <button>Войти</button>
</label><br/>
<?php
  if (isset($_COOKIE["login_error"])) {
    echo "Неверный логин или пароль.";
  }
?>
</form>

</html>
