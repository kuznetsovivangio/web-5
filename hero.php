<!doctype html>

<html lang="ru">

<head>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1"/>
  <link rel="stylesheet" type="text/css" href="style.css">
  <title>Index</title>
</head>

<?php
  if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['id_hero'])) {
    setcookie("id_hero", $_POST['id_hero']);
  };
  $id_cur_hero = $_COOKIE['id_hero'];
  if (isset($_COOKIE["wait"]) && $_COOKIE["wait"]>0) {
    setcookie("wait", $_COOKIE["wait"]-1);
    header("Refresh: 0");
  }
  
  $db_host = 'localhost'; $db_user = 'u20967'; $db_password = '7306510';
  $bd = new PDO("mysql:host=$db_host; dbname=$db_user", $db_user, $db_password, array(PDO::ATTR_PERSISTENT => true));
?>

<body>
<form method="post" class="content" action="" autocomplete="off">
  <label>
    Имя:<br/>
    <input name="name" <?PHP echo "value=", $bd->query("SELECT name FROM heroes WHERE id_hero=$id_cur_hero")->fetch()[0];?> />
  </label><br/>
  <label >
    Текстовое поле email:<br/>
    <input name="email" <?PHP echo "value=", $bd->query("SELECT email FROM heroes WHERE id_hero=$id_cur_hero")->fetch()[0];?> />
  </label><br/>
  <label>
  Год рождения:<br/>
  <select size="5" name="birthyear">
    <option value="1996" <?php if ($bd->query("SELECT birthyear FROM heroes WHERE id_hero=$id_cur_hero")->fetch()[0]==1996) echo "selected"?> >1996</option>
    <option value="1997" <?php if ($bd->query("SELECT birthyear FROM heroes WHERE id_hero=$id_cur_hero")->fetch()[0]==1997) echo "selected"?> >1997</option>
    <option value="1998" <?php if ($bd->query("SELECT birthyear FROM heroes WHERE id_hero=$id_cur_hero")->fetch()[0]==1998) echo "selected"?> >1998</option>
    <option value="1999" <?php if ($bd->query("SELECT birthyear FROM heroes WHERE id_hero=$id_cur_hero")->fetch()[0]==1999) echo "selected"?> >1999</option>
    <option value="2000" <?php if ($bd->query("SELECT birthyear FROM heroes WHERE id_hero=$id_cur_hero")->fetch()[0]==2000) echo "selected"?> >2000</option>
    <option value="2001" <?php if ($bd->query("SELECT birthyear FROM heroes WHERE id_hero=$id_cur_hero")->fetch()[0]==2001) echo "selected"?> >2001</option>
    <option value="2002" <?php if ($bd->query("SELECT birthyear FROM heroes WHERE id_hero=$id_cur_hero")->fetch()[0]==2002) echo "selected"?> >2002</option>
  </select>
  </label><br/>
  Пол:<br/>
    <label><input type="radio" name="sex" value="M" <?php if ($bd->query("SELECT sex FROM heroes WHERE id_hero=$id_cur_hero")->fetch()[0]=="M") echo "checked"?>/> Мужской</label>
    <label><input type="radio" name="sex" value="F" <?php if ($bd->query("SELECT sex FROM heroes WHERE id_hero=$id_cur_hero")->fetch()[0]=="F") echo "checked"?>/> Женский</label><br/>
  Количество конечностей:<br/>
    <label><input type="radio" name="limb" value="1" <?php if ($bd->query("SELECT limb FROM heroes WHERE id_hero=$id_cur_hero")->fetch()[0]==1) echo "checked"?> /> 1 </label>
    <label><input type="radio" name="limb" value="2" <?php if ($bd->query("SELECT limb FROM heroes WHERE id_hero=$id_cur_hero")->fetch()[0]==2) echo "checked"?> /> 2 </label>
    <label><input type="radio" name="limb" value="4" <?php if ($bd->query("SELECT limb FROM heroes WHERE id_hero=$id_cur_hero")->fetch()[0]==4) echo "checked"?> /> 4 </label>
    <label><input type="radio" name="limb" value="6" <?php if ($bd->query("SELECT limb FROM heroes WHERE id_hero=$id_cur_hero")->fetch()[0]==6) echo "checked"?> /> 6 </label>
    <label><input type="radio" name="limb" value="8" <?php if ($bd->query("SELECT limb FROM heroes WHERE id_hero=$id_cur_hero")->fetch()[0]==8) echo "checked"?> /> 8 </label><br/>
  Сверхспособности:<br/>
  <select name="powers[]" multiple="multiple" autocomplete="off">
    <option value="1" <?php if (!empty($bd->query("SELECT id_power FROM powers_sets WHERE id_hero=$id_cur_hero AND id_power=1")->fetch())) echo "selected='selected'"?> >Бессмертие</option>
    <option value="2" <?php if (!empty($bd->query("SELECT id_power FROM powers_sets WHERE id_hero=$id_cur_hero AND id_power=2")->fetch())) echo "selected='selected'"?> >Прохождение сквозь стены</option>
    <option value="3" <?php if (!empty($bd->query("SELECT id_power FROM powers_sets WHERE id_hero=$id_cur_hero AND id_power=3")->fetch())) echo "selected='selected'"?> >Левитация</option>
    <option value="4" <?php if (!empty($bd->query("SELECT id_power FROM powers_sets WHERE id_hero=$id_cur_hero AND id_power=4")->fetch())) echo "selected='selected'"?> >Отсутствие потребности во сне</option>
  </select><br/>
  <label>
    Биография:<br/>
    <textarea name="biography"><?php echo $bd->query("SELECT biography FROM heroes WHERE id_hero=$id_cur_hero")->fetch()[0] ?></textarea>
  </label><br/>
  <input name="save" type="submit" value="Сохранить изменения"/>
  <input name="delete" type="submit" value="Удалить героя"/>
  <input name="back" type="submit" value="Назад"/>
  </br></br>
</form>

<?php
  header('Content-Type: text/html; charset=UTF-8');
  setlocale(LC_ALL, 'Russian_Russia.65001');
  
  $check = true;
  if ($_SERVER['REQUEST_METHOD'] == 'POST')
  try{
    if (isset($_POST['back'])) {
      header("Location: editor.php");
      exit;
    } else {
    if (isset($_POST['delete'])) {
      $id_hero = $_COOKIE["id_hero"];
      $bd->exec("DELETE FROM powers_sets WHERE id_hero=$id_hero");
      $bd->exec("DELETE FROM heroes_sets WHERE id_hero=$id_hero");
      $bd->exec("DELETE FROM heroes WHERE id_hero=$id_hero");
      header("Location: editor.php");
      exit;
    } else {
    if (empty($_POST['name']) || preg_match("/^[A-Z]{1}[a-z]{0,}$/", $_POST['name'])==0 ||
        empty($_POST['email']) || preg_match("/^[\-_A-Za-z0-9]{1,}@[A-Za-z0-9]{1,}.[A-Za-z]{2,}$/", $_POST['email'])==0) {
        $check = false;
    }
    
    if ($check==false) {
      header("Refresh: 0");
      exit;
    }
    
    $heroes = $bd->prepare("UPDATE heroes SET name = ?, email = ?, birthyear = ?, sex = ?, limb = ?, biography = ? WHERE id_hero = $id_hero");
    $heroes -> execute([$_POST['name'], $_POST['email'], $_POST['birthyear'], $_POST['sex'], $_POST['limb'], $_POST['biography']]);
    $id_hero = $bd->lastInsertId();

    $powers_sets = $bd->prepare("UPDATE powers_sets SET id_hero = ?, id_power = ? WHERE id_hero = $id_hero");
    for ($i = 0; $i<count($_POST["powers"]); $i++) {
      $powers_sets -> execute([$id_hero, $_POST['powers'][$i]]);
    }

    if (empty($_COOKIE['id_user'])) {
      $login = substr(str_shuffle('abdefhiknrstyzABDEFGHKNQRSTYZ'), 0, 8);
      $password = substr(str_shuffle('abdefhiknrstyzABDEFGHKNQRSTYZ1234567890'), 0, 12);
      $users = $bd->prepare("UPDATE users SET login = ?, password = ? WHERE id_hero = $id_hero");
      $users -> execute([$login, $password]);
      $id_user = $bd->lastInsertId();
      setcookie("id_user", $id_user, time()+31536000);
      setcookie("new_user", true, time()+31536000);
    } else {
      $id_user = $_COOKIE["id_user"];
    }

    $heroes_sets = $bd->prepare("UPDATE heroes_sets SET id_hero = ?, id_user = ? WHERE id_hero = $id_hero");
    $heroes_sets -> execute([$id_hero, $id_user]);

    setcookie("wait", 1);
    header("Refresh: 0");
    setcookie("complete", true);
    }}
  } catch(PDOException $e){
    echo "Произошла ошибка при сохранении изменений!";
    print('Error : ' . $e->getMessage());
  }
?>
</body>


</html>