CREATE TABLE users (
  id_user int(10) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  login varchar(8) NOT NULL,
  password varchar(12) NOT NULL
);

CREATE TABLE heroes (
  id_hero int(10) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  name varchar(64) NOT NULL,
  email varchar(64) NOT NULL,
  birthyear int(4) NOT NULL,
  sex char(1) NOT NULL,
  limb char(1) NOT NULL,
  biography varchar(256)
);

CREATE TABLE powers_sets (
  id_bond int(10) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  id_hero int(10) NOT NULL,
  id_power int(2) NOT NULL
);

CREATE TABLE heroes_sets (
  id_hero int(10) NOT NULL PRIMARY KEY,
  id_user int(10) NOT NULL
);

CREATE TABLE powers (
  id_power int(10) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  meaning varchar(64) NOT NULL
);

INSERT INTO powers SET meaning = "Бессмертие";
INSERT INTO powers SET meaning = "Прохождение сквозь стены";
INSERT INTO powers SET meaning = "Левитация";
INSERT INTO powers SET meaning = "Отсутствие потребности во сне";

DROP TABLE powers CASCADE;
DROP TABLE heroes_sets CASCADE;
DROP TABLE powers_sets CASCADE;
DROP TABLE heroes CASCADE;
DROP TABLE users CASCADE;

mysql -uu20967 -p7306510
use u20967


SELECT id_user FROM users WHERE login='kSfENhni' AND password='Hdh2B4kNQsS7';

Пользователь
-id пользователя
-Логин
-Пароль

Герой
-id героя
-Имя
-Мыло
-ДР
-Пол
-Конечности
-Биография

Наборы суперсил
-id связи
-Номер героя
-id суперсилы

Набор героев
-id связи
-id пользователя, которому принадлежит герой
-id героя

Таблица суперсил
-id суперсилы
-Пояснение значения